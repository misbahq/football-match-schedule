package com.example.xan.mytab.model

import com.google.gson.annotations.SerializedName

data class League (
    @SerializedName("strHomeTeam")
    var homeTeamName: String? = null,

    @SerializedName("idHomeTeam")
    var homeTeamId: String? = null,

    @SerializedName("intHomeScore")
    var homeTeamScore: String? = null,

    @SerializedName("strAwayTeam")
    var awayTeamName: String? = null,

    @SerializedName("idAwayTeam")
    var awayTeamId: String? = null,

    @SerializedName("intAwayScore")
    var awayTeamScore: String? = null,

    @SerializedName("dateEvent")
    var dateEvent: String? = null
    )