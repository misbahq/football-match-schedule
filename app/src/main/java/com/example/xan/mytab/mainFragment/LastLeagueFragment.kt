package com.example.xan.mytab.mainFragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.xan.mytab.api.ApiRespository
import com.example.xan.mytab.constant.Commont
import com.example.xan.mytab.model.League
import com.google.gson.Gson
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.ctx

class LastLeagueFragment: Fragment(), FragmentView {

    private lateinit var presenter: FragmentPresenter
    private val leagueID: Int = 4399
    private val fragmentUi = FragmentUi<Fragment>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val createView = fragmentUi.createView(AnkoContext.create(ctx, this))
        fragmentUi.setContent()

        val request = ApiRespository()
        val gson = Gson()
        presenter = FragmentPresenter(this, request, gson)
        presenter.getLeagueList(leagueID, Commont.LAST_LEAGUE)

        return createView
    }

    override fun showLoading() {
        fragmentUi.showLoading()
    }

    override fun hideLoading() {
        fragmentUi.hideLoading()
    }

    override fun showLeagueList(data: List<League>) {
        fragmentUi.clear()
        fragmentUi.addAll(data)
    }

}