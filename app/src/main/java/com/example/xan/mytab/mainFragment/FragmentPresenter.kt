package com.example.xan.mytab.mainFragment

import com.example.xan.mytab.api.ApiRespository
import com.example.xan.mytab.api.TheSportDBApi
import com.example.xan.mytab.constant.Commont
import com.example.xan.mytab.model.LeagueResponse
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class FragmentPresenter(private val view: LastLeagueFragment,
                        private val apiRepository: ApiRespository,
                        private val gson: Gson) {


    fun getLeagueList(id: Int?, type: String) {
        view.showLoading()
        doAsync {
            var url = ""

            when(type) {
                Commont.LAST_LEAGUE -> url = TheSportDBApi.getLastMatch(id)
                Commont.NEXT_LEAGUE -> url = TheSportDBApi.getNextMatch(id)
            }

            val data = gson.fromJson(apiRepository
                .doRequest(url),
                LeagueResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showLeagueList(data.events)
            }
        }
    }
}