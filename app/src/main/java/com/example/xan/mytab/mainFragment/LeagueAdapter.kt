package com.example.xan.mytab.mainFragment

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.example.xan.mytab.model.League
import org.jetbrains.anko.AnkoContext

class LeagueAdapter(private val leagues: List<League>, private val listener: (League) -> Unit)
    : RecyclerView.Adapter<LeagueViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeagueViewHolder {
        return LeagueViewHolder(LeagueUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = leagues.size

    override fun onBindViewHolder(holder: LeagueViewHolder, position: Int) {
        holder.bindItem(leagues[position], listener)
    }
}

