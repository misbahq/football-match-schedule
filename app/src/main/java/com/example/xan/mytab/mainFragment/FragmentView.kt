package com.example.xan.mytab.mainFragment

import com.example.xan.mytab.model.League

interface FragmentView {
    fun showLoading()
    fun hideLoading()
    fun showLeagueList(data: List<League>)
}