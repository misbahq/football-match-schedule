package com.example.xan.mytab.mainFragment

import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.xan.mytab.R
import com.example.xan.mytab.R.id.*
import org.jetbrains.anko.*

class LeagueUI: AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                padding = dip(16)
                orientation = LinearLayout.VERTICAL

                textView {
                    id = date_event
                    gravity = Gravity.CENTER_HORIZONTAL
                    textColor = Color.parseColor("#008577")
                }

                relativeLayout {
                    lparams(matchParent, wrapContent)

                    textView {
                        id = vs
                        text = context.getString(R.string.vs)
                    }.lparams(wrapContent, wrapContent) {
                        centerHorizontally()
                        horizontalMargin = dip(8)
                        topMargin = dip(3)
                    }

                    textView {
                        id = home_team_score
                        textSize = 18f //sp
                        setTypeface(typeface, Typeface.BOLD)
                    }.lparams {
                        leftMargin = dip(8)
                        leftOf(vs)
                    }

                    textView {
                        id = home_team_name
                        gravity = Gravity.RIGHT
                        maxLines = 1
                        textSize = 18f //sp
                    }.lparams {
                        horizontalMargin = dip(8)
                        leftOf(home_team_score)
                    }

                    textView {
                        id = away_team_score
                        textSize = 18f //sp
                        setTypeface(typeface, Typeface.BOLD)
                    }.lparams {
                        rightMargin = dip(8)
                        rightOf(vs)
                    }

                    textView {
                        id = away_team_name
                        maxLines = 1
                        textSize = 18f //sp
                    }.lparams {
                        horizontalMargin = dip(8)
                        rightOf(away_team_score)
                    }
                }
            }
        }
    }
}