package com.example.xan.mytab.api

import android.net.Uri
import com.example.xan.mytab.BuildConfig

object TheSportDBApi {

    /**
     * Past league by ID
     * https://www.thesportsdb.com/api/v1/json/1/eventspastleague.php?id=4399
     */
    fun getLastMatch(id: Int?): String {
        return Uri.parse(BuildConfig.BASE_URL).buildUpon()
            .appendPath("api")
            .appendPath("v1")
            .appendPath("json")
            .appendPath(BuildConfig.TSDB_API_KEY)
            .appendPath("eventspastleague.php")
            .appendQueryParameter("id", id.toString())
            .build()
            .toString()
    }

    /**
     * Next league by ID
     * https://www.thesportsdb.com/api/v1/json/1/eventsnextleague.php?id=4399
     */
    fun getNextMatch(id: Int?): String {
        return Uri.parse(BuildConfig.BASE_URL).buildUpon()
            .appendPath("api")
            .appendPath("v1")
            .appendPath("json")
            .appendPath(BuildConfig.TSDB_API_KEY)
            .appendPath("eventsnextleague.php")
            .appendQueryParameter("id", id.toString())
            .build()
            .toString()
    }
}