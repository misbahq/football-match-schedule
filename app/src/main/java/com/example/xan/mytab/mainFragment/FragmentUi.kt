package com.example.xan.mytab.mainFragment

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ProgressBar
import com.example.xan.mytab.detail.DetailActivity
import com.example.xan.mytab.model.League
import com.example.xan.mytab.util.invisible
import com.example.xan.mytab.util.visible
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class FragmentUi<T>: AnkoComponent<T> {
    private var leagues: MutableList<League> = mutableListOf()
    private var listLeague: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var adapter: LeagueAdapter? = null

    override fun createView(ui: AnkoContext<T>) = with(ui) {
        verticalLayout {
            relativeLayout {
                lparams(width = matchParent, height = matchParent)

                listLeague = recyclerView {
                    lparams(width = matchParent, height = wrapContent)

                    layoutManager = LinearLayoutManager(ctx)
                }

                progressBar = progressBar {
                }.lparams {
                    centerInParent()
                }
            }
        }
    }

    fun setContent() {
        adapter = LeagueAdapter(leagues) {

        }
        listLeague?.adapter = adapter
    }

    fun showLoading() {
        progressBar?.visible()
    }

    fun hideLoading() {
        progressBar?.invisible()
    }

    fun clear() {
        leagues.clear()
    }

    fun addAll(data: List<League>) {
        leagues.addAll(data)
        adapter?.notifyDataSetChanged()
    }
}