package com.example.xan.mytab.model

data class LeagueResponse(val events: List<League>)