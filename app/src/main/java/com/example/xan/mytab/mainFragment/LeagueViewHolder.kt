package com.example.xan.mytab.mainFragment

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.xan.mytab.R.id.*
import com.example.xan.mytab.model.League
import org.jetbrains.anko.find

class LeagueViewHolder(private val containerView: View): RecyclerView.ViewHolder(containerView) {
    private val homeTeamName: TextView = containerView.find(home_team_name)
    private val awayTeamName: TextView = containerView.find(away_team_name)
    private val homeTeamScore: TextView = containerView.find(home_team_score)
    private val awayTeamScore: TextView = containerView.find(away_team_score)
    private val dateEvent: TextView = containerView.find(date_event)

    fun bindItem(leagues: League, listener: (League) -> Unit) {
        homeTeamName.text = leagues.homeTeamName
        awayTeamName.text = leagues.awayTeamName
        homeTeamScore.text = leagues.homeTeamScore
        awayTeamScore.text = leagues.homeTeamScore
        dateEvent.text = leagues.dateEvent
        containerView.setOnClickListener { listener(leagues) }
    }
}